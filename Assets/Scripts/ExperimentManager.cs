﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperimentManager : MonoBehaviour
{
    [SerializeField]
    Target[] targets;
    [SerializeField]
    LayerMask targetLayer;
    [SerializeField]
    string fileName;
    [SerializeField]
    Logger log;
    Target activeTarget;
    [SerializeField]
    private Test[] testPlan;

    List<DataPoint> DataPointList = new List<DataPoint>();

    private IndexDifficulty currentID;
    private Test currentTest;
    private float timer;
    private int inputs;
    private int tests;

    int Inputs
    {
        get
        {
            return inputs;
        }
        set
        {
            if (value >= currentTest.inputs)
            {
                Tests++;
                inputs = 0;
            }
            else
            {
                inputs = value;
            }
        }
    }

    int Tests
    {
        get
        {
            return tests;
        }
        set
        {
            if(value < testPlan.Length)
            {
                tests = value;
                currentTest = testPlan[tests];
                NextID();
            }
            else
            {
                EndTest();
            }
        }
    }

    public void StartTest()
    {
        DataPointList.Clear();
        log.Setup(fileName);
        activeTarget = ChangeActiveTarget();
        currentTest = testPlan[0];
        Inputs = 0;
        timer = 0;
        Tests = 0;
        InputManager.OnPoint += Point;
        NextID();
    }

    public void EndTest()
    {
        log.LogToCSV(DataPointList);
        UIManager.instance.showUI();
        InputManager.OnPoint -= Point;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
    }

    void NextID()
    {
        currentID = new IndexDifficulty(currentTest.id, currentTest.distance);
        InitializeTargets(currentID.distance, currentID.width);
    }

    /// <summary>
    /// subscribe this during testing
    /// </summary>
    /// <param name="pos"></param>
    void Point(Vector3 pos)
    {
        if (Physics.Raycast(pos, Vector3.forward, Mathf.Infinity, targetLayer))
        {
            DataPointList.Add(new DataPoint(currentID.ID, timer, 0));
            Inputs++;
            activeTarget.HitTarget();
        }
        else
        {
            activeTarget.MissTarget();
        }
        timer = 0;
        activeTarget = ChangeActiveTarget();
    }

    /// <summary>
    /// Initialisiert die beiden Ziele mit Distanz D und Width W
    /// </summary>
    /// <param name="distance"></param>
    /// <param name="width"></param>
    void InitializeTargets(float distance, float width)
    { 
        float newX = distance / 2 + width / 2;
        targets[0].newPosition(-newX, width);
        targets[1].newPosition(newX, width);
    }

    Target ChangeActiveTarget()
    {
        if (targets[0].Active)
        {
            targets[0].Active = false;
            targets[1].Active = true;
            return targets[1];
        }
        else
        {
            targets[0].Active = true;
            targets[1].Active = false;
            return targets[0];
        }
    }
}
