﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cursor : MonoBehaviour
{
    InputManager inputManager;
    [SerializeField]
    Image cursor;
    // Start is called before the first frame update
    void Start()
    {
        inputManager = FindObjectOfType<InputManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (inputManager.inputDevice == InputDevice.Mouse)
        {
            cursor.rectTransform.position = Input.mousePosition;
        }else if (inputManager.inputDevice == InputDevice.Joystick)
        {
            cursor.rectTransform.position = new Vector3(Screen.width/2+inputManager.joystickAxisX * Screen.width * 0.5f,Screen.height/2+ -inputManager.joystickAxisY * Screen.height * 0.5f, -10);
        }
    }
}
