﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

public enum InputDevice { Mouse, Joystick}
public class InputManager : MonoBehaviour
{
    [SerializeField]
    public InputDevice inputDevice = InputDevice.Mouse;
    SerialPort stream = new SerialPort("COM4", 9600);

    public static event InputEventHandler OnPoint;
    public delegate void InputEventHandler(Vector3 position);

    public float joystickAxisX;
    public float joystickAxisY;
    public bool buttonDown = false;
    public bool buttonUp = false;
    public bool Button
    {
        get
        {
            return button;
        }
        set
        {
            if(value && value != button)
            {
                buttonDown = true;
            }else if(!value && value != button)
            {
                buttonUp = true;
            }
            button = value;
        }
    }
    private bool button;


    private void Start()
    {
        stream.Open();
    }
    // Update is called once per frame
    void Update()
    {
        if (inputDevice==InputDevice.Mouse)
        {
            GetMouseInput();
        }else if (inputDevice == InputDevice.Joystick)
        {
            ReadSerial();
            GetJoystickInput();
        }
    }

    void GetMouseInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnPoint?.Invoke(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
    }

    void GetJoystickInput()
    {
        if (buttonDown)
        {
            Debug.Log("press");
            OnPoint?.Invoke(Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2+0.5f * Screen.width * joystickAxisX,Screen.height/2 + 0.5f * Screen.height * -joystickAxisY, -10)));
        }
    }

    void ReadSerial()
    {
        buttonDown = false;
        buttonUp = false;
        string[] values = stream.ReadLine().Split('/');
        int buttonState = int.Parse(values[0]);
        if (buttonState == 1)
        {
            Button = true;
        }
        else
        {
            Button = false;
        }
        joystickAxisX = float.Parse(values[1])/100;
        joystickAxisY = float.Parse(values[2])/100;
        Debug.Log(button + " " + joystickAxisX + " " + joystickAxisY);
    }
}
