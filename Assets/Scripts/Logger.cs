﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;
using UnityEngine;

public class Logger : MonoBehaviour
{
    public string delimiter = ";";
    private StreamWriter writer;

    public void Setup(string testName)
    {
        string filePath = Application.dataPath + "/Resources/" + testName +".csv";
        writer = new StreamWriter(filePath);
        string[] head = { "ID", "MoveTime" };
        WriteLineToCSV(head);
    }

    public void LogToCSV(List<DataPoint> dataSet)
    {
        foreach (DataPoint data in dataSet)
        {
            string[] lineData = { data.id.ToString(), data.ms.ToString()};
            WriteLineToCSV(lineData);
        }
    }

    private void WriteLineToCSV(string[] lineData)
    {
        string line = string.Join(delimiter, lineData);
        writer.WriteLine(line);
        writer.Flush();
    }
}
