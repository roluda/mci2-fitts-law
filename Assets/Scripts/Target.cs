﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public float blinkTime;
    Renderer render;
    private bool active;
    public bool Active
    {
        get
        {
            return active;
        }
        set
        {
            if (value == true)
            {
                StopAllCoroutines();
                gameObject.layer = 8;
                render.material.color = Color.white;
            }
            else
            {
                gameObject.layer = 0;
            }
            active = value;
        }
    }

    // Start is called before the first frame update
    private void Awake()
    {
        render = GetComponent<Renderer>();
        render.material.color = Color.gray;
    }

    /// <summary>
    /// sets the target to position x with width w
    /// </summary>
    /// <param name="x"></param>
    /// <param name="width"></param>
    public void newPosition(float x, float w)
    {
        transform.position = new Vector3(x, 0, 0);
        transform.localScale = new Vector3(w, transform.localScale.y, transform.localScale.z);
    }

    public void HitTarget()
    {
        StartCoroutine(BlinkColor(Color.green, blinkTime));
    }

    public void MissTarget()
    {
        StartCoroutine(BlinkColor(Color.red, blinkTime));
    }

    IEnumerator BlinkColor(Color blink, float time)
    {
        render.material.color = blink;
        yield return new WaitForSeconds(time);
        render.material.color = Color.gray;
    }
}
