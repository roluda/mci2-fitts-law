﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct Test
{
    public float id;
    public float distance;
    public float inputs;
}
