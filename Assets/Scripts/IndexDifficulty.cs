﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct IndexDifficulty
{
    public IndexDifficulty(float id, float dis)
    {
        distance = dis;
        width = Mathf.Pow(2, -id + Mathf.Log(2 * dis, 2));
    }
    public float distance;
    public float width;
    public float ID
    {
        get
        {
            return Mathf.Log((2 * distance) / width, 2);
        }
    }
}
