﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct DataPoint
{
    public float id, ms, error;
    public DataPoint(float ID, float MS, float err)
    {
        id = ID;
        ms = MS;
        error = err;
    }
}
