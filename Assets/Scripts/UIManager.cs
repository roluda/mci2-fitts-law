﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public TextMeshProUGUI countdown;
    public ExperimentManager exMan;

    public GameObject[] hideableObjects;

    private void Start()
    {
        if(instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    public void hideUI()
    {
        foreach(GameObject go in hideableObjects)
        {
            go.SetActive(false);
        }
    }

    public void showUI()
    {
        foreach (GameObject go in hideableObjects)
        {
            go.SetActive(true);
        }
    }

    public void StartTest()
    {
        hideUI();
        StartCoroutine(StartCountdown());
    }

    IEnumerator StartCountdown()
    {
        countdown.gameObject.SetActive(true);
        countdown.text = "3";
        yield return new WaitForSeconds(1);
        countdown.text = "2";
        yield return new WaitForSeconds(1);
        countdown.text = "1";
        yield return new WaitForSeconds(1);
        countdown.text = "start";
        exMan.StartTest();
        yield return new WaitForSeconds(.5f);
        countdown.gameObject.SetActive(false);
    }
}
